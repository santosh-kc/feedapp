//
//  PhotoListViewController.swift
//  FeedApp
//
//  Created by Santosh Kc on 8/6/19.
//  Copyright © 2019 Smartmobe. All rights reserved.
//

import UIKit

class PhotoListViewController: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var searchbar: UISearchBar! {
        didSet {
            self.searchbar.delegate = self
        }
    }
    
    
    func configure(photoListViewModel: PhotoListViewModel) {
        self.photoListViewModel = photoListViewModel
    }
    
    var photoListViewModel: PhotoListViewModel!
    
    lazy var activityIndicator: UIActivityIndicatorView = {
        let activityIndicator = UIActivityIndicatorView(style: .gray)
        activityIndicator.center = view.center
        activityIndicator.hidesWhenStopped = true
        view.addSubview(activityIndicator)
        return activityIndicator
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
    
        let tap = UITapGestureRecognizer(target: self, action: #selector(viewTapped(sender:)))
        tap.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tap)
        
        photoListViewModel.loadData() { [weak self] in
            self?.newDataRecieved()
        }
    }
}

extension PhotoListViewController {
    fileprivate func newDataRecieved() {
        debugPrint("total count: \(photoListViewModel.data.count)")
        self.collectionView.reloadData()
    }
    
    @objc  func viewTapped(sender: UITapGestureRecognizer) {
        if self.searchbar.isFirstResponder {
            self.searchbar.resignFirstResponder()
        }
    }
}

extension PhotoListViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return photoListViewModel.data.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let viewModel = PhotoViewCellViewModelImpl(photoDTO: photoListViewModel.data[indexPath.item])
        let cell = PhotoListCollectionViewCell.deque(from: collectionView, for: indexPath, with: viewModel)
        return cell
    }
}

extension PhotoListViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.frame.width / 2 - 10
        return CGSize(width: width, height: width)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 2, left: 2, bottom: 2, right: 2)
    }
}

extension PhotoListViewController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if let text = searchbar.text {
            self.photoListViewModel.search(text: text) {[weak self] in
                self?.newDataRecieved()
            }
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.isEmpty {
            self.photoListViewModel.loadData { [weak self] in
                self?.newDataRecieved()
            }
        }
    }
}
