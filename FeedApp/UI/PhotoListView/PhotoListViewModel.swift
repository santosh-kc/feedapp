//
//  ModelLayer.swift
//  FeedApp
//
//  Created by Santosh Kc on 8/6/19.
//  Copyright © 2019 Smartmobe. All rights reserved.
//

import Foundation

protocol PhotoListViewModel {
    var data: [PhotoDTO] { get }
    var error: CustomError? {get}
    func loadData(resultsLoaded: @escaping () -> Void)
    func search(text: String, resultLoaded: @escaping () -> Void)
}

class PhotoListViewModelImpl: PhotoListViewModel {
    
    var data: [PhotoDTO] = []
    var error: CustomError?
    fileprivate var networkLayer: NetworkLayer
    fileprivate var translationLayer: TranslationLayer
    
    init(networkLayer: NetworkLayer, translationLayer: TranslationLayer) {
        self.networkLayer = networkLayer
        self.translationLayer = translationLayer
    }
    
    func loadData(resultsLoaded: @escaping () -> Void) {
        let request = APIRequest(url: Api.latestAssestsURL, method: .get)
        networkLayer.loadFromServer(request: request) { (data, error) in
            guard let data = data else {
                self.data = []
                self.error = error
                resultsLoaded()
                return
            }
            let dtos = self.translationLayer.createAssestsDTOsFromJsonData(data)
            self.data = dtos
            self.error = error
            resultsLoaded()
        }
    }
    
    func search(text: String, resultLoaded: @escaping () -> Void) {
        let apiRequest = APIRequest(url: Api.searchQueryURL, method: .get, parameters: ["query": text], headers: nil)
        networkLayer.loadFromServer(request: apiRequest) { (data, error) in
            guard let data = data else {
                self.data = []
                self.error = error
                resultLoaded()
                return
            }
            let dtos = self.translationLayer.createAssestsDTOsFromJsonData(data)
            self.data = dtos
            self.error = error
            resultLoaded()
        }
    }
}
