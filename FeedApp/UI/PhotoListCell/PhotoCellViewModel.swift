//
//  PhotoCellViewModel.swift
//  FeedApp
//
//  Created by Santosh Kc on 8/6/19.
//  Copyright © 2019 Smartmobe. All rights reserved.
//

import Foundation

protocol PhotoViewCellViewModel {
    var url: String {get}
}

class PhotoViewCellViewModelImpl: PhotoViewCellViewModel {
    var photoDTO: PhotoDTO
    
    var url: String {return photoDTO.url}
    
    init(photoDTO: PhotoDTO) {
        self.photoDTO = photoDTO
    }
    
}
