//
//  PhotoListCollectionViewCell.swift
//  FeedApp
//
//  Created by Santosh Kc on 8/6/19.
//  Copyright © 2019 Smartmobe. All rights reserved.
//

import UIKit
import Kingfisher

class PhotoListCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    
    func configure(photoViewCellViewModel: PhotoViewCellViewModel) {
        imageView.kf.setImage(with: URL(string: photoViewCellViewModel.url))
    }
}

extension PhotoListCollectionViewCell {
    static var cellId: String {
        return "PhotoCollectionViewCell"
    }
    
    static func deque(from collectionView: UICollectionView, for indexPath: IndexPath, with viewModel: PhotoViewCellViewModel) -> PhotoListCollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: PhotoListCollectionViewCell.cellId, for: indexPath) as? PhotoListCollectionViewCell
        cell!.configure(photoViewCellViewModel: viewModel)
        return cell!
    }
}
