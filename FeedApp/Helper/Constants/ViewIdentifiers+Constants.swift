//
//  ViewIdentifiers+Constants.swift
//  FeedApp
//
//  Created by Santosh Kc on 8/6/19.
//  Copyright © 2019 Smartmobe. All rights reserved.
//

import Foundation

struct ViewIdentifier {
    static var photoListViewController: String { return "PhotoListViewController" }
    static var photoDetailViewController: String { return "PhotoDetailViewController" }
}

struct Storyboard {
    static var main: String { return "Main" }
}
