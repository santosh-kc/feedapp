//
//  Network+Constants.swift
//  FeedApp
//
//  Created by Santosh Kc on 8/6/19.
//  Copyright © 2019 Smartmobe. All rights reserved.
//

import Foundation

struct Api {
    static var latestAssestsURL: String { return "http://www.splashbase.co/api/v1/images/latest"}
    static var searchQueryURL: String { return "http://www.splashbase.co/api/v1/images/search"}
}

