//
//  TranslationLayer.swift
//  FeedApp
//
//  Created by Santosh Kc on 8/6/19.
//  Copyright © 2019 Smartmobe. All rights reserved.
//

import Foundation
import CoreData

protocol TranslationLayer {
    func createAssestsDTOsFromJsonData(_ data: Data) -> [PhotoDTO]
    func toUnsavedCoreData(from dtos: [PhotoDTO], with context: NSManagedObjectContext) -> [Photo]
    func toPhotoDTOs(from photos:[Photo]) -> [PhotoDTO]
}

class TranslationLayerImpl: TranslationLayer {
    
    fileprivate var photoTranslator: PhotoTranslator
    
    init(photoTranslator: PhotoTranslator) {
        self.photoTranslator = photoTranslator
    }
    
    func toUnsavedCoreData(from dtos: [PhotoDTO], with context: NSManagedObjectContext) -> [Photo] {
        let photos = dtos.compactMap { dto  in
            self.photoTranslator.translate(from: dto, with: context)
        }
        return photos
    }
    
    func createAssestsDTOsFromJsonData(_ data: Data) -> [PhotoDTO] {
        let decoder = JSONDecoder()
        do {
            let imags =  try decoder.decode(Root.self, from: data)
            return imags.images
        } catch {
            print(error)
            return []
        }
    }
    
    func toPhotoDTOs(from photos: [Photo]) -> [PhotoDTO] {
        let dtos = photos.compactMap { photo in
            self.photoTranslator.translate(from: photo)
        }
        return dtos
    }
}
