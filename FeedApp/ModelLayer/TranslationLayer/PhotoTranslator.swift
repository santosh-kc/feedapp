//
//  PhotoTranslator.swift
//  FeedApp
//
//  Created by santosh kc on 8/16/19.
//  Copyright © 2019 Smartmobe. All rights reserved.
//

import Foundation
import  CoreData

protocol PhotoTranslator {
    func translate(from photo: Photo?) -> PhotoDTO?
    func translate(from dto: PhotoDTO?, with context: NSManagedObjectContext) -> Photo?
}

class PhotoTranslatorImpl: PhotoTranslator {
    
    func translate(from photo: Photo?) -> PhotoDTO? {
        guard let photo = photo else {return nil}
        
        return PhotoDTO(id: Int(photo.id),
                        sourceId: Int(photo.sourceId),
                        url: photo.url!,
                        largeUrl: photo.largeUrl!)
        
    }
    
    func translate(from dto: PhotoDTO?, with context: NSManagedObjectContext) -> Photo? {
        
        guard let dto = dto else {return nil}
        
        let photo = Photo(context: context)
        photo.id = Int64(dto.id)
        if let sourceId = dto.sourceId {
            photo.sourceId = Int64(sourceId)
        }
        photo.url = dto.url
        photo.largeUrl = dto.largeUrl
        
        return photo
    }
}

