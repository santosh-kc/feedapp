//
//  NetworkLayer.swift
//  FeedApp
//
//  Created by Santosh Kc on 8/6/19.
//  Copyright © 2019 Smartmobe. All rights reserved.
//

import Alamofire

protocol NetworkLayer {
    func loadFromServer(request: APIRequest, finished: @escaping (Data?, CustomError?) -> Void)
}

class NetworkLayerImpl: NetworkLayer {
    func loadFromServer(request: APIRequest, finished: @escaping (Data?, CustomError?) -> Void) {
        print("loading data from server")
        print("url : \(request.url)")
        AF.request(request.url,
                          method: request.method,
                          parameters: request.parameters,
                          headers: request.headers).responseJSON { response in
                            guard let data = response.data else {
                                finished(nil, CustomError.genericError)
                                return
                            }
                            finished(data, nil)
        }
    }
}
