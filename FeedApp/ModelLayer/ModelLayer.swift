//
//  ModelLayer.swift
//  FeedApp
//
//  Created by santosh kc on 8/22/19.
//  Copyright © 2019 Smartmobe. All rights reserved.
//

import Foundation

enum Source: String {
    case local, network
}

typealias PhotosAndSourceBlock = (Source, [PhotoDTO]) -> Void

protocol ModelLayer {
    
}

class ModelLayerImpl: ModelLayer {
    
}
