//
//  PhotoDTO.swift
//  FeedApp
//
//  Created by Santosh Kc on 8/6/19.
//  Copyright © 2019 Smartmobe. All rights reserved.
//

import Foundation

struct Root: Codable {
    var images: [PhotoDTO]
}

struct PhotoDTO: Codable {
    var id: Int
    var sourceId: Int?
    var url: String
    var largeUrl: String
    
    init(id: Int, sourceId:Int, url: String, largeUrl: String) {
        self.id = id
        self.sourceId = sourceId
        self.url = url
        self.largeUrl = largeUrl
    }
    
    enum CodingKeys: String, CodingKey {
        case id
        case sourceId = "source_id"
        case url = "url"
        case largeUrl = "large_url"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try container.decode(Int.self, forKey: .id)
        self.sourceId = try container.decodeIfPresent(Int.self, forKey: .sourceId)
        self.url = try container.decode(String.self, forKey: .url)
        self.largeUrl = try container.decode(String.self, forKey: .largeUrl)
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(self.id, forKey: .id)
        try container.encode(self.sourceId, forKey: .sourceId)
        try container.encode(self.url, forKey: .url)
        try container.encode(self.largeUrl, forKey: .largeUrl)
    }
}
