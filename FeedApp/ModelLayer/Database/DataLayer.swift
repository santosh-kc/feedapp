//
//  DataLayer.swift
//  FeedApp
//
//  Created by santosh kc on 8/16/19.
//  Copyright © 2019 Smartmobe. All rights reserved.
//

import Foundation
import CoreData

typealias PhotoBlock = ([Photo]) -> Void

protocol DataLayer {
    func loadFromDB(finished: PhotoBlock)
    func save(dtos: [PhotoDTO], translationLayer: TranslationLayer, finished: @escaping () -> Void)
}

class DataLayerImpl: DataLayer {
    
    private var persistantContainer: NSPersistentContainer
    
    var mainContext: NSManagedObjectContext {
        return persistantContainer.viewContext
    }
    
    init(persistentContainer: NSPersistentContainer = CoreDataManager.shared.persistantContainer) {
        self.persistantContainer = persistentContainer
    }
    
    
    func loadFromDB(finished: ([Photo]) -> Void) {
        let photos = self.loadPhotosFromDB()
        finished(photos)
    }
    
    func save(dtos: [PhotoDTO], translationLayer: TranslationLayer, finished: @escaping () -> Void) {
        clearOldResults()
        _ = translationLayer.toUnsavedCoreData(from: dtos, with: mainContext)
        try! mainContext.save()
        finished()
    }
    
    
}

extension DataLayerImpl {
    
    fileprivate func loadPhotosFromDB() -> [Photo] {
        let sortOn = NSSortDescriptor(key: "id", ascending: true)
        let fetchRequest: NSFetchRequest<Photo> = Photo.fetchRequest()
        fetchRequest.sortDescriptors = [sortOn]
        let photos = try! persistantContainer.viewContext.fetch(fetchRequest)
        return photos
    }
    
    fileprivate func clearOldResults() {
        print("clearing old results")
        let fetchRequest: NSFetchRequest<NSFetchRequestResult> = Photo.fetchRequest()
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
        
        try! persistantContainer.persistentStoreCoordinator.execute(deleteRequest, with: persistantContainer.viewContext)
        persistantContainer.viewContext.reset()
    }
}
