//
//  CoredataManager.swift
//  FeedApp
//
//  Created by santosh kc on 8/24/19.
//  Copyright © 2019 Smartmobe. All rights reserved.
//

import CoreData

class CoreDataManager {
    private init() {}
    static let shared = CoreDataManager()
    
    lazy  var persistantContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "FeedApp")
        container.viewContext.automaticallyMergesChangesFromParent = true
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        
        return container
    }()
    
}
