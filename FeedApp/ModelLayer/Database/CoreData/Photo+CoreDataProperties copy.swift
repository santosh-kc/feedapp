//
//  Photo+CoreDataProperties.swift
//  
//
//  Created by santosh kc on 8/16/19.
//
//

import Foundation
import CoreData


extension Photo {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Photo> {
        return NSFetchRequest<Photo>(entityName: "Photo")
    }

    @NSManaged public var id: Int64
    @NSManaged public var sourceId: Int64
    @NSManaged public var url: String?
    @NSManaged public var largeUrl: String?

}
