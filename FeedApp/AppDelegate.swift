//
//  AppDelegate.swift
//  FeedApp
//
//  Created by Santosh Kc on 8/5/19.
//  Copyright © 2019 Smartmobe. All rights reserved.
//

import UIKit
import CoreData

//@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    static var dependencyRegistry: DependencyRegistry!
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        debugPrint("Running from AppDelegate")
        makeInitialViewController()
        return true
    }
}

extension AppDelegate {
    fileprivate func makeInitialViewController() {
        self.window = UIWindow(frame: UIScreen.main.bounds)
        let vc = AppDelegate.dependencyRegistry.makePhotoListVC()
        self.window?.rootViewController = vc
        self.window?.makeKeyAndVisible()
    }
}

