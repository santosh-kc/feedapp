//
//  SwinjectStoryboard+extensions.swift
//  FeedApp
//
//  Created by Santosh Kc on 8/6/19.
//  Copyright © 2019 Smartmobe. All rights reserved.
//

import SwinjectStoryboard

extension SwinjectStoryboard {
    
    public static func setup() {
        if AppDelegate.dependencyRegistry == nil {
            AppDelegate.dependencyRegistry = DependencyRegistryImpl(container: defaultContainer)
        }
        
        let dependencyRegistry: DependencyRegistry = AppDelegate.dependencyRegistry
        dependencyRegistry.container.storyboardInitCompleted(PhotoListViewController.self) { (r, VC) in
            VC.configure(photoListViewModel: r.resolve(PhotoListViewModel.self)!)
        }
    }
}
