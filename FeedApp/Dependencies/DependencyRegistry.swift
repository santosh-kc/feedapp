//
//  DependencyRegistry.swift
//  FeedApp
//
//  Created by Santosh Kc on 8/6/19.
//  Copyright © 2019 Smartmobe. All rights reserved.
//

import Swinject

protocol DependencyRegistry {
    var container: Container {get}
    
    func makePhotoCell(for collectionView: UICollectionView, at indexpath:IndexPath, assests: PhotoDTO) -> PhotoListCollectionViewCell
    func makePhotoListVC() -> PhotoListViewController
}

class DependencyRegistryImpl: DependencyRegistry {
    
    var container: Container
    
    init(container: Container) {
        Container.loggingFunction = nil
        
        self.container = container
        registerModel()
        registerViewModels()
        registerViewControllers()
    }
    
    func registerModel() {
        container.register(PhotoTranslator.self) { _  in
            return PhotoTranslatorImpl()
        }
        container.register(NetworkLayer.self) { _ in
            return NetworkLayerImpl()
        }
        container.register(TranslationLayer.self) { r in
            return TranslationLayerImpl(photoTranslator: r.resolve(PhotoTranslator.self)!)
        }
//        container.register(TranslationLayer.self) { _ in
//            return TranslationLayerImpl()
//        }
    }
    
    func registerViewModels() {
        container.register(PhotoListViewModel.self) { r in
            return PhotoListViewModelImpl(networkLayer: r.resolve(NetworkLayer.self)!, translationLayer: r.resolve(TranslationLayer.self)!)
        }
        
        container.register(PhotoViewCellViewModel.self) { (resolver, photoDTO: PhotoDTO) in
            return PhotoViewCellViewModelImpl(photoDTO: photoDTO)
        }
    }
    
    func registerViewControllers() {
        container.register(PhotoListViewController.self) { _ in
            let photoListVC = UIStoryboard.init(name: Storyboard.main, bundle: nil).instantiateViewController(withIdentifier: ViewIdentifier.photoListViewController) as! PhotoListViewController
            return photoListVC
        }
    }
    
    
    func makePhotoCell(for collectionView: UICollectionView, at indexpath: IndexPath, assests: PhotoDTO) -> PhotoListCollectionViewCell {
        let viewModel = container.resolve(PhotoViewCellViewModel.self, argument: assests)!
        let cell = PhotoListCollectionViewCell.deque(from: collectionView, for: indexpath, with: viewModel)
        return cell
    }
    
    func makePhotoListVC() -> PhotoListViewController {
        return container.resolve(PhotoListViewController.self)!
    }
}
