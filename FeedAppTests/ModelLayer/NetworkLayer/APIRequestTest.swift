//
//  APIRequest.swift
//  FeedAppTests
//
//  Created by Santosh Kc on 8/6/19.
//  Copyright © 2019 Smartmobe. All rights reserved.
//

import XCTest
import Alamofire
@testable import FeedApp

class APIRequestTest: XCTestCase {
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    func testAPIRequstInitializerSetCorrectly() {
        let url = "http://test.url"
        let method = HTTPMethod.get
        let parameters = ["KEY":"TEST KEY"]
        let headers = HTTPHeaders([HTTPHeader(name: "HEADER", value: "TEST_HEADER")])
        
        
        let apiRequest = APIRequest(url: url, method: method, parameters: parameters, headers: headers)
        XCTAssertEqual(url, apiRequest.url)
        XCTAssertEqual(apiRequest.method, method)
        XCTAssertEqual(parameters, apiRequest.parameters! as! [String: String])
        XCTAssertEqual(headers.count, 1)
        XCTAssertEqual(headers.first!, headers.first!)
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
}
