//
//  PhotoTranslatorTests.swift
//  FeedAppTests
//
//  Created by santosh kc on 8/17/19.
//  Copyright © 2019 Smartmobe. All rights reserved.
//

import XCTest
import CoreData
import  Swinject
@testable import FeedApp

class PhotoTranslatorTests: XCTestCase {
    
    var sut: PhotoTranslator!
    let container = Container()
    var mockPersistentContainer: NSPersistentContainer!
    
    override func setUp() {
        container.register(PhotoTranslator.self) { _  in
            return PhotoTranslatorImpl()
        }
        
        sut = container.resolve(PhotoTranslator.self)
        mockPersistentContainer = MockManageCobjectContext().persistantContainer
        
    }
    
  
    
    func testPhotoTranslatedToPhotoDTOCorrectly() {
        let photo = Photo(context: mockPersistentContainer.viewContext)
        photo.id = 1
        photo.sourceId = 100
        photo.url = "http://example.ccom/example.png"
        photo.largeUrl = "http://example.ccom/large_example.png"
        
        let dto = sut.translate(from: photo)
        XCTAssertEqual(dto?.id, Int(photo.id))
        XCTAssertEqual(dto?.sourceId, Int(photo.sourceId))
        XCTAssertEqual(dto?.url, photo.url)
        XCTAssertEqual(dto?.largeUrl, photo.largeUrl)
    }
    
    func testPhotoTranslaedToNilIfNilIsPassed() {
        let dto = sut.translate(from: nil)
        XCTAssertNil(dto)
    }
    
    func testPhotoDTOIsTranslatedProperly() {
        let photoDto = PhotoDTO(id: 2,
                                sourceId: 200,
                                url: "http://example.ccom/example.png",
                                largeUrl: "http://example.ccom/large_example.png")
        let photo = sut.translate(from: photoDto, with: mockPersistentContainer.viewContext)
        XCTAssertEqual(Int(photo!.id), photoDto.id)
        XCTAssertEqual(Int(photo!.sourceId), photoDto.sourceId)
        XCTAssertEqual(photo?.url, photoDto.url)
        XCTAssertEqual(photo?.largeUrl, photoDto.largeUrl)
    }
    
    func testTranslatePhotoDtoShouldReturmNilOnNilDto() {
        let photo = sut.translate(from: nil, with: mockPersistentContainer.viewContext)
        XCTAssertNil(photo)
    }
    
    override func tearDown() {
        sut = nil
        mockPersistentContainer = nil
        super.tearDown()
    }
}


class MockManageCobjectContext {
    
    private var managedObjectModel: NSManagedObjectModel = {
        let managedObjectModel = NSManagedObjectModel.mergedModel(from: [Bundle.main])!
        return managedObjectModel
    }()
    
    lazy var persistantContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "FeedAppUnitTesting", managedObjectModel: self.managedObjectModel)
        let description = NSPersistentStoreDescription()
        description.type = NSInMemoryStoreType
        description.shouldAddStoreAsynchronously = false
        container.persistentStoreDescriptions = [description]
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            precondition(description.type == NSInMemoryStoreType)
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
}
