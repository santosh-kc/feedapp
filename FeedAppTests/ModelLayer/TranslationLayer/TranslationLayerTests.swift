//
//  TranslationLayerTests.swift
//  FeedAppTests
//
//  Created by Santosh Kc on 8/7/19.
//  Copyright © 2019 Smartmobe. All rights reserved.
//

import XCTest
import  Swinject
@testable import FeedApp

class TranslationLayerTests: XCTestCase {
    
    var sut:TranslationLayer!
    let container = Container()
    
    override func setUp() {
        container.register(PhotoTranslator.self) { _  in
            return PhotoTranslatorImpl()
        }
        container.register(TranslationLayer.self) { r in
            return TranslationLayerImpl(photoTranslator: r.resolve(PhotoTranslator.self)!)
        }
        sut = container.resolve(TranslationLayer.self)!
    }
    
    func testEmptyDataShouldShowZeroPhotoDTOs() {
        let url = Bundle(for: type(of: self)).url(forResource: "empty_latest", withExtension: "json")
        let jsonData = try? Data(contentsOf: url!)
        let dtos = sut.createAssestsDTOsFromJsonData(jsonData!)
        XCTAssertEqual(dtos.count, 0)
    }
    
    func testDataShouldBeTranslatedCorrectly() {
        let url = Bundle(for: type(of: self)).url(forResource: "latest", withExtension: "json")
        let jsonData = try! Data(contentsOf: url!)
        let dtos = sut.createAssestsDTOsFromJsonData(jsonData)
        XCTAssertEqual(dtos.count, 10)
        XCTAssertEqual(dtos.first!.id, 8798)
        XCTAssertEqual(dtos.first!.largeUrl, "https://splashbase.s3.amazonaws.com/newoldstock/large/tumblr_ph8vgdJV2r1sfie3io1_1280.jpg")
        XCTAssertEqual(dtos.first!.sourceId, nil)
        XCTAssertEqual(dtos.first!.url, "https://splashbase.s3.amazonaws.com/newoldstock/regular/tumblr_ph8vgdJV2r1sfie3io1_1280.jpg")
    }
    
    

    override func tearDown() {
        sut = nil
    }

}
