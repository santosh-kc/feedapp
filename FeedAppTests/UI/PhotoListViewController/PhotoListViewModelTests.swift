//
//  PhotoListViewModelTests.swift
//  FeedAppTests
//
//  Created by Santosh Kc on 8/7/19.
//  Copyright © 2019 Smartmobe. All rights reserved.
//

import XCTest
import Swinject
import Alamofire
@testable import FeedApp

class PhotoListViewModelTests: XCTestCase {
    
    let container = Container()
    var sut: PhotoListViewModel!
    
    override func setUp() {
        container.register(NetworkLayer.self) { r  in
            return MockNetworkLayerImpl()
            }.inObjectScope(.container)
        container.register(PhotoTranslator.self) { _ in
            return PhotoTranslatorImpl()
        }
        container.register(TranslationLayer.self) { r  in
            return TranslationLayerImpl(photoTranslator: r.resolve(PhotoTranslator.self)!)
        }
        container.register(PhotoListViewModel.self) { r in
            return PhotoListViewModelImpl(networkLayer: r.resolve(NetworkLayer.self)!, translationLayer: r.resolve(TranslationLayer.self)!)
        }
        sut = container.resolve(PhotoListViewModel.self)
    }
    
    func testLoadFromServerMethodIsCalled() {
        
        let completedExpectation = expectation(description: "Completed")
        
        sut.loadData() {
            completedExpectation.fulfill()
        }
        let networkLayer = self.container.resolve(NetworkLayer.self) as! MockNetworkLayerImpl
        XCTAssertEqual(networkLayer.isMethodCalled, true)
        waitForExpectations(timeout: 0.1, handler: nil)
    }
    
    func testRequestIsSentWithProperURL() {
        let completedExpectation = expectation(description: "Completed")
        
        sut.loadData() {
            completedExpectation.fulfill()
        }
        let networkLayer = self.container.resolve(NetworkLayer.self) as! MockNetworkLayerImpl
        XCTAssertEqual(networkLayer.apiRequest?.url, "http://www.splashbase.co/api/v1/images/latest")
        waitForExpectations(timeout: 0.1, handler: nil)
    }
    
    func testEmptyArrayIsAssignedOnError() {
        let completedExpectation = expectation(description: "Completed")
        let networkLayer = self.container.resolve(NetworkLayer.self) as! MockNetworkLayerImpl
        networkLayer.data = nil
        sut.loadData {
            completedExpectation.fulfill()
        }
        XCTAssertEqual(sut.data.count, 0)
        XCTAssertNotNil(sut.error)
        waitForExpectations(timeout: 0.1, handler: nil)
    }
    
    func testCorrectDataArrayIsSetOnSuccess() {
        let completedExpectation = expectation(description: "Completed")
        let networkLayer = self.container.resolve(NetworkLayer.self) as! MockNetworkLayerImpl
        let url = Bundle(for: type(of: self)).url(forResource: "latest", withExtension: "json")
        let jsonData = try! Data(contentsOf: url!)
        networkLayer.data = jsonData
        sut.loadData {
            completedExpectation.fulfill()
        }
        XCTAssertNil(sut.error)
        XCTAssertEqual(sut.data.count, 10)
        waitForExpectations(timeout: 0.1, handler: nil)
    }
    
    func testSearchCorrectApiRequestIsSent() {
        let completedExpectation = expectation(description: "Completed")
        let networkLayer = self.container.resolve(NetworkLayer.self) as! MockNetworkLayerImpl
        sut.search(text: "test search") {
            completedExpectation.fulfill()
        }
        XCTAssertEqual(networkLayer.apiRequest!.parameters!["query"] as! String, "test search")
        XCTAssertEqual(networkLayer.apiRequest!.method, HTTPMethod.get)
        XCTAssertEqual(networkLayer.apiRequest!.url, Api.searchQueryURL)
        
        waitForExpectations(timeout: 0.1, handler: nil)
    }
    
    override func tearDown() {
        sut = nil
    }
    
}

class MockNetworkLayerImpl: NetworkLayer {
    
    var isMethodCalled = false
    var apiRequest:APIRequest?
    var data:Data?
    var error: CustomError?
    
    func loadFromServer(request: APIRequest, finished: @escaping (Data?, CustomError?) -> Void) {
        self.isMethodCalled = true
        self.apiRequest = request
        if let d = self.data {
            finished(d, nil)
        } else {
            finished(nil, CustomError.genericError)
        }
    }
}
