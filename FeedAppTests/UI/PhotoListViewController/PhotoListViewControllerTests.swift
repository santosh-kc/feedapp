//
//  PhotoListViewControllerTests.swift
//  FeedAppTests
//
//  Created by Santosh Kc on 8/7/19.
//  Copyright © 2019 Smartmobe. All rights reserved.
//

import XCTest
import Swinject
@testable import FeedApp

class PhotoListViewControllerTests: XCTestCase {
    
    let container = Container()
    var sut: PhotoListViewController!
    
    override func setUp() {
        appDelegate.window = UIWindow(frame: UIScreen.main.bounds)
        appDelegate.window!.makeKeyAndVisible()
        container.register(PhotoListViewController.self) { _ in
            let photoListVC = UIStoryboard.init(name: Storyboard.main, bundle: nil).instantiateViewController(withIdentifier: ViewIdentifier.photoListViewController) as! PhotoListViewController
            return photoListVC
        }
        container.register(NetworkLayer.self) { r  in
            return MockNetworkLayerImpl()
            }.inObjectScope(.container)
        container.register(PhotoTranslator.self) { _  in
            return PhotoTranslatorImpl()
        }
        container.register(TranslationLayer.self) { r  in
            return TranslationLayerImpl(photoTranslator: r.resolve(PhotoTranslator.self)!)
        }
        container.register(PhotoListViewModel.self) { r in
            return PhotoListViewModelImpl(networkLayer: r.resolve(NetworkLayer.self)!, translationLayer: r.resolve(TranslationLayer.self)!)
        }
        sut = container.resolve(PhotoListViewController.self)
        sut.configure(photoListViewModel: container.resolve(PhotoListViewModel.self)!)
    }
    
    func testCollectionViewDatasourceAndDelegateAreProperlySet() {
        _ = sut.view
        XCTAssertTrue(sut.collectionView.dataSource is PhotoListViewController)
        XCTAssertTrue(sut.collectionView.delegate is PhotoListViewController)
    }
    
    func testShouldShowTrueDataOnLoad() {
        let mockNetworkLayer = container.resolve(NetworkLayer.self) as! MockNetworkLayerImpl
        let url = Bundle(for: type(of: self)).url(forResource: "latest", withExtension: "json")
        let jsonData = try! Data(contentsOf: url!)
        mockNetworkLayer.data = jsonData
        _ = sut.view
        XCTAssertEqual(sut.collectionView.numberOfItems(inSection: 0), 10)
    }
    
    func testShouldDequeCorrectCollectionView() {
        let mockNetworkLayer = container.resolve(NetworkLayer.self) as! MockNetworkLayerImpl
        let url = Bundle(for: type(of: self)).url(forResource: "latest", withExtension: "json")
        let jsonData = try! Data(contentsOf: url!)
        mockNetworkLayer.data = jsonData
        _ = sut.view
        XCTAssertTrue(sut.collectionView.dataSource?.collectionView(sut.collectionView, cellForItemAt:  IndexPath(item: 0, section: 0)) is PhotoListCollectionViewCell)
        //        XCTAssertTrue(sut.collectionView.cellForItem(at: IndexPath(item: 0, section: 0)) is PhotoListCollectionViewCell)
    }
    
    func testSearchBarDelegateIsSetProperly() {
        _ = sut.view
        XCTAssertTrue(sut.searchbar.delegate is PhotoListViewController)
    }
    
    func testTapGestureIsAdded() {
        _ = sut.view
        XCTAssertTrue(sut.view.gestureRecognizers!.contains(where: { (gr) -> Bool in
            return gr is UITapGestureRecognizer
        }))
        XCTAssertFalse(sut.searchbar.isFirstResponder)
    }
    
    func testKeyBoardHidesOnViewTapped() {
        _ = sut.view
        sut.searchbar.becomeFirstResponder()
        let gs = sut.view.gestureRecognizers!.first as! UITapGestureRecognizer
        sut.viewTapped(sender: gs)
        XCTAssertFalse(sut.searchbar.isFirstResponder)
    }
    
    override func tearDown() {
        appDelegate.window = nil
        sut = nil
    }
}

extension PhotoListViewControllerTests {
    var appDelegate: TestAppDelegate {
        return UIApplication.shared.delegate as! TestAppDelegate
    }
}
