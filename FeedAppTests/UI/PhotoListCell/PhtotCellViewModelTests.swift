//
//  PhtotCellViewModelTests.swift
//  FeedAppTests
//
//  Created by Santosh Kc on 8/7/19.
//  Copyright © 2019 Smartmobe. All rights reserved.
//

import XCTest
import Swinject
@testable import FeedApp

class PhtotCellViewModelTests: XCTestCase {
    
    var sut: PhotoViewCellViewModel!
    let container = Container()
    var testDTO: PhotoDTO!
    
    override func setUp() {
        testDTO = PhotoDTO(id: 100, sourceId: 2, url: "http://test.url", largeUrl: "https://large.url")
        container.register(PhotoViewCellViewModel.self) { r in
            return PhotoViewCellViewModelImpl(photoDTO: self.testDTO)
        }
        sut = container.resolve(PhotoViewCellViewModel.self)!
    }
    
    func testURLisSetCorrectly(){
        XCTAssertEqual(sut.url, testDTO.url)
    }

    override func tearDown() {
        sut = nil
        testDTO = nil
    }

}
